#
# Copyright 2021 - IMT Atlantique Bretagne
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Process for using rules to test changes in a document.
"""

import sys
import json
import inkex

from collections import defaultdict

from .utils import Within, element_types, intersection

class BadExamRule(ValueError):
    """A bad exam rule"""

class Exam(list):
    """A list of rules loaded from a document"""
    def __init__(self, doc_was, doc_then):
        self.doc_was = doc_was
        self.doc_then = doc_then
        self.failures = []

    def append(self, rule):
        if isinstance(rule, ExamRubric):
            super().append(rule)
        elif isinstance(rule, list):
            for item in rule:
                self.append(rule)
        elif isinstance(rule, dict):
            try:
                self.append(ExamRubric(**rule))
            except TypeError as err:
                raise BadExamRule(f"Bad exam rubric inputs: {err}")
        elif isinstance(rule, str):
            rule = rule.strip()
            if not rule.startswith('['):
                rule = '[' + rule.strip(',') + ']'
            try:
                for item in json.loads(rule):
                    self.append(item)
            except json.decoder.JSONDecodeError as err:
                raise BadExamRule(f"Could not parse exam rule: {err}\n```\n{rule}\n```\n")
        else:
            kind = type(rule).__name__
            raise BadExamRule(f"Can't add {kind} of rule, unknown type.")

    def run_test(self):
        """Run each test in the exam and return the total score"""
        self.failures = []
        self.success = []
        for rule in self:
            result, elem, crit = rule.run_test(self.doc_was, self.doc_then)
            if not result:
                self.failures.append((rule, elem, crit))
            else:
                self.success.append((rule, elem))

    def has_passed(self):
        """Return true if this exam passes the rules"""
        if not self.failures:
            self.run_test()
        return not self.failures

class ExamRubric:
    def __init__(self, relabel=None, match=(), criteria=(), error_text=None):
        self.matcher = None
        self.label = relabel
        self.rules = []
        self.matcher = ObjectMatcher(match)
        self.criteria = [ExamCriteria(*r) for r in criteria if isinstance(r, (list, tuple))]
        self.error_text = error_text

    def run_test(self, doc_was, doc_then):
        """Runs this test on the current ontext in the exam"""
        elem = None
        if self.label:
            for item, score in self.matcher.get_by_label(doc_then, self.label):
                elem = item
        if elem is None:
            elem = self.matcher.get_first(doc_then)
            if elem is None:
                return (False, None, None)
            if self.label:
                elem.label = self.label

        # XXX It might be possible to add in here a way of getting the
        # element by id in doc_was and therefore allow delta tests too
        for crit in self.criteria:
            elem._is_selected = elem in doc_then.selection
            if not crit.test(elem):
                return (False, elem, crit)
        return (True, elem, None)

class ExamCriteria:
    def __init__(self, kind, *bits):
        self.kind = kind
        self.args = []
        self.kwargs = {}
        self.error_text = None
        for bit in bits:
            if isinstance(bit, list):
                self.args = bit
            elif isinstance(bit, dict):
                self.kwargs = bit
            elif isinstance(bit, (bool, int, float)):
                self.kwargs['value'] = bit
            elif isinstance(bit, str):
                self.error_text = bit
        # Set the testing function and be flexible
        self.func = getattr(self, f'test_{kind}')
        if self.kwargs and not self.args:
            self.func = getattr(self, f'test_{kind}_kw', self.func)

    def test(self, elem):
        """Test this element against the critiria"""
        return self.func(elem, *self.args, **self.kwargs)

    @staticmethod
    def test_selected(elem, value):
        """See if the element is selected or not"""
        return getattr(elem, '_is_selected', False) == value

    @staticmethod
    def test_size(elem, width, height, delta=1.0):
        """Size is wrong"""
        return ExamCriteria.test_size_kw(elem, width=width, height=height, delta=delta)

    @staticmethod
    def test_pos(elem, left, top, delta=1.0):
        """Position is wrong"""
        return ExamCriteria.test_size_kw(elem, left=left, top=top, delta=delta)

    @staticmethod
    def test_size_kw(elem, delta=1.0, **kwargs):
        """Size is wrong"""
        bbox = elem.shape_box()
        for label in kwargs:
            if isinstance(kwargs[label], str) and kwargs[label].startswith('#'):
                # Allow user to specify a different side to align it to.
                o_id, *rest = kwargs[label].split(':')
                against = rest[0] if rest else label
                # Get element from the same document
                other = elem.root.getElementById(o_id)
                if other is None:
                    return False
                # Use the same transform step as above
                p = other.composed_transform()
                obbox = other.shape_box()
                kwargs[label] = getattr(obbox, against)
            if getattr(bbox, label) not in Within.range(kwargs[label], delta):
                return False
        return True

    @staticmethod
    def test_pos_kw(elem, **kwargs):
        """Position is wrong"""
        return ExamCriteria.test_size_kw(elem, **kwargs)

    @staticmethod
    def test_center(elem, cx, cy, delta=1.0):
        """Center in wrong position"""
        return ExamCriteria.test_size_kw(elem, center_x=cx, center_y=cy, delta=delta)

    @staticmethod
    def test_overlap(elem, **objects):
        """Object does not overlap"""
        box = elem.shape_box()
        for oid in objects:
            target = elem.root.getElementById(oid)
            if target is None:
                return False
            if intersection(box, target.shape_box()) not in Within.range(objects[oid]):
                return False
        return True

    @staticmethod
    def test_path(elem, subpaths=None, nodes=None, count=None):
        """Shape is wrong"""
        path = elem.path
        csp = path.to_superpath()
        if subpaths is not None:
            subpaths = Within.range(subpaths, delta=0)
            if len(csp) not in subpaths:
                return False
        if count is not None and nodes is None:
            nodes = [count]
        if nodes is not None:
            if isinstance(nodes, list):
                for x, n in enumerate(nodes):
                    if x >= len(csp) or len(csp[x]) - 1 not in Within.range(n, delta=0):
                        return False
            elif len(path) != int(nodes):
                return False
        return True

    @staticmethod
    def test_lpe(elem, effect=None, **kwargs):
        effect_id = elem.get('inkscape:path-effect', None)
        if effect_id is None:
            return False
        effect_elem = elem.root.getElementById(effect_id)
        if effect_elem is None:
            return False
        if effect is not None and effect_elem.get('effect') != effect:
            return False
        for attr in kwargs:
            _test = getattr(ExamCriteria, f"test_lpe_{attr}", ExamCriteria.test_lpe_attr)
            if not _test(elem, effect_elem, effect_elem.get(attr, None), kwargs[attr]):
                return False
        return True

    @staticmethod
    def test_lpe_attr(elem, lpe, a, b):
        """Basic attribute testing, which can be overloaded for custom testing"""
        return a == b

    def auto_error_text(self, elem):
        """Generate an error text from the element"""
        return self.func.__doc__ or "Unknown error!"

class ObjectMatcher:
    def __init__(self, matchings):
        self.mat = matchings
        self.needed = len(self.mat)

    @staticmethod
    def _get_descendants(svg):
        for item in svg.descendants():
            if ObjectMatcher._valid_object(item):
                yield item

    @staticmethod
    def _valid_object(elem):
        for anc in elem.ancestors():
            if isinstance(anc, inkex.Defs):
                return False
            if not anc.is_sensitive():
                return False
        return True

    def get_objects(self, svg):
        """Return the matched objects based on the matchings, returns a generator of score, element pairs"""
        total = defaultdict(int)
        for key, val in self.mat.items():
            try:
                fn = getattr(self, f'get_by_{key}')
            except AttributeError:
                fn = None
            if fn is None:
                raise BadExamRule(f"Not such matching test '{key}'")
            for elem, score in fn(svg, val):
                total[elem] += score

        for elem in sorted(total, key=lambda k: total[k], reverse=True):
            yield (elem, total[elem])

    def get_first(self, svg):
        """Return the highest matching object"""
        for elem, score in self.get_objects(svg):
            if score >= self.needed:
                return elem

    @staticmethod
    def get_by_type(svg, val):
        """Find all elements of a specific type"""
        if val not in element_types:
            raise KeyError(f"Unknown element type: '{val}'")
        for elem in ObjectMatcher._get_descendants(svg):
            if isinstance(elem, element_types[val]):
                yield (elem, 1.0)

    @staticmethod
    def get_by_size(svg, size, x='width', y='height'):
        """Find all elements that are aprox the given size"""
        x_range, y_range = Within.xy_range(*size, unit=svg.unit, dd=1.0)
        for elem in ObjectMatcher._get_descendants(svg):
            try:
                bbox = elem.shape_box(transform=elem.composed_transform())
            except AttributeError:
                continue
            if getattr(bbox, x) in x_range and getattr(bbox, y) in y_range:
                # Could adjust score based on proximity to test.
                yield (elem, 1.0)

    @staticmethod
    def get_by_pos(svg, pos):
        return ObjectMatcher.get_by_size(svg, pos, x='left', y='top')

    @staticmethod
    def get_by_center(svg, pos):
        return ObjectMatcher.get_by_size(svg, pos, x='center_x', y='center_y')

    @staticmethod
    def get_by_label(svg, label):
        for elem in svg.descendants():
            if elem.label == label or elem.get_id() == label:
                if ObjectMatcher._valid_object(elem):
                    yield (elem, 10.0) # High score, because it's certain
