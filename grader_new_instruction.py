#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 IMT (France)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# Author: Martin Owens <doctormo@geek-2.com>
#
"""
Create a new class based on the template file.
"""

import inkex
from inkex.elements import load_svg
from inkex.units import parse_unit

from grader import __version__
from grader.core import GraderMixin
from grader.diff import SvgChanges

class NewMoocInstruction(GraderMixin, inkex.EffectExtension):
    """Create a new Mooc Instruction"""

    def add_arguments(self, pars):
        pars.add_argument("--text", help="Instruction text")

    def effect(self):
        # Perform any initalisation tasks here
        self.clear_errors()

        # 0. Remove meta instruction if it exists.
        elem = self.svg.getElementById('meta-instruction')
        if elem is not None:
            elem.delete()

        # 1. Get difference between previous state and the current state (if possible)
        old_doc = self.load_state()
        if old_doc:
            diff = SvgChanges(old_doc.getroot(), self.svg)

            # 2. Generate a basic set of instructions for this difference
            mr_instructions = self.write_machine_instructions(diff)
        else:
            mr_instructions = ""

        # 3. Generate a new instruction text block
        self.add_instruction('ins-%d', self.options.text, mr_instructions)

        # 3. Insert a description into the instruction
        self.save_state()

    def write_machine_instructions(self, diff):
        """Attempts to write a basic set of changes based on the diff"""
        return "\n".join(self._wmi(diff))

    def _wmi(self, diff):
        return "[", "]"
        #for d in diff.get_all("CREATED"):
        #    if not self.is_mooc_element(d.elem):
        #        yield "+" + d.elem.TAG
        #for d in diff.get_all("DELETED"):
        #    if not self.is_mooc_element(d.prev_elem):
        #        yield "-" + d.prev_elem.get_id()

if __name__ == '__main__':
    NewMoocInstruction().run()
