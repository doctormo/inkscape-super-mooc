# coding=utf-8
#
# Copyright (C) 2021 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA.
#
"""Test creating finishing a class"""

from datetime import datetime
from inkex.tester import ComparisonMixin, TestCase

import grader_finalise_class
from grader_finalise_class import FinaliseMoocClass

def now():
    return datetime(2000, 1, 1, 0, 0, 0)

class NewClassTest(ComparisonMixin, TestCase):
    mocks = [(grader_finalise_class, 'now', now)]
    effect_class = FinaliseMoocClass
    compare_file = 'test_class_end.svg'
    comparisons = [
        (),
    ]
