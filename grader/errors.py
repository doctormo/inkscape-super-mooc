#
# Copyright 2021 - IMT Atlantique Bretagne
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""The errors component of the grader system."""

from inkex.paths import Line
from inkex.elements import ShapeElement

class ErrorsMixin:
    """Add this to manage error text and lines"""
    def add_error(self, text, point):
        """Adds an error and an arrow to point at it"""
        if isinstance(point, ShapeElement):
            point = point.shape_box(True).minimum
        # Get next available arrow
        for err in self.svg.getElementsByClass('error_empty'):
            # Set the data to right place
            self._point_error(err, point, text, True)
            # Mark it as used
            err.set('class', 'error')
            return

    def clear_errors(self):
        """Clear all the errors"""
        # Loop through all errors
        for err in self.svg.getElementsByClass('error'):
            # Clear all elements
            self._point_error(err)
            # Mark it as available
            err.set('class', 'error_empty')

    def _point_error(self, err, point=(0, 0), text='No problem', display=False):
        # set the points to target
        pointer = err.findone('svg:path')
        self._show(pointer, point is not None)
        if point is not None:
            path = pointer.path
            path[-1] = Line(*point).transform(-err.composed_transform())
            pointer.path = path
        # Set the tspan to text
        tspan = err.findone('svg:text/svg:tspan')
        tspan.text = text

        _ex_tspan = err.findone('svg:text/svg:tspan/svg:tspan')
        if _ex_tspan is not None:
            _ex_tspan.delete()

        # Make it visible
        self._show(err, display)
