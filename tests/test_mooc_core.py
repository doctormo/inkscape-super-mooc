# coding=utf-8

from grader.core import GraderMixin

from inkex.tester import TestCase
from inkex import load_svg

class MyExtension(GraderMixin):
    def __init__(self, svg):
        self.document = load_svg(svg)
        self.svg = self.document.getroot()

class CoreTest(TestCase):
    """Testing core functions"""
    def setUp(self):
        self.doc = MyExtension(self.data_file('test_class.svg'))
        super().setUp()

    def test_set_title(self):
        self.doc.set_title('Hello')
        self.assertIn(b'Hello', self.doc.svg.tostring())

    def test_state_saving(self):
        """Test saving and loading the state"""
        self.assertEqual(self.doc.load_state(), None)
        self.doc.set_title('Blueberry')
        self.doc.save_state() # Saves the current state into the desc
        self.assertIn('svg', self.doc.svg.desc)
        self.assertIn('Blueberry', self.doc.svg.desc)
        self.doc.set_title('Apple')

        state_doc = self.doc.load_state().getroot()
        self.assertIn(b'Blueberry', state_doc.tostring())
        self.assertNotIn(b'Apple', state_doc.tostring())

        self.doc.delete_state()
        self.assertEqual(self.doc.load_state(), None)
        self.assertIn(b'Apple', self.doc.svg.tostring())
        self.assertNotIn(b'Blueberry', self.doc.svg.tostring())

