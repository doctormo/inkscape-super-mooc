#
# Copyright 2021 - IMT Atlantique Bretagne
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Structures for handling the difference between two documents.
"""

from collections import defaultdict, OrderedDict
from difflib import SequenceMatcher

import inkex

class MissingLayerError(KeyError):
    """Caused by a layer being missing"""

class SvgChanges(defaultdict):
    """
    A record of the changes between two xml structures.

    If layer is none, it returns a difference between the entire
    document. With layer, it reports only layer differences.
    """
    def __init__(self, svg_orig, svg_next, layer=None):
        super().__init__(DiffElement)
        if layer:
            layer_orig = svg_orig.getElementById(layer)
            layer_next = svg_next.getElementById(layer)
            if layer_orig is None or layer_next is None:
                raise MissingLayerError(f"The layer '{layer}' is missing.")
            self._xmldiff(layer_orig, layer_next)
        else:
            self._xmldiff(svg_orig, svg_next)

        # Make sure different object has element id
        for elem_id, d_el in self.items():
            if d_el.elem is None and d_el.prev_elem is None:
                raise ValueError(f"Different Element not set: {d_el}")
            d_el.elem_id = elem_id

    def _xmldiff(self, xml_was, xml_then):
        if xml_was.tag != xml_then.tag:
            raise DiffError("Tags are not the same, and that's not how diffs work.")

        elem_id = xml_was.get_id()
        if elem_id != xml_then.get_id():
            raise DiffError("Elements have different ids, how did it come to this!")

        for name, value in xml_was.attrib.items():
            if name not in xml_then.attrib:
                self.append_attr(elem_id, name, xml_was.attrib[name], None)
            elif xml_then.attrib.get(name) != value:
                self.append_attr(elem_id, name, xml_was.attrib.get(name), xml_then.attrib.get(name))
        for name, value in xml_then.attrib.items():
            if name not in xml_was.attrib:
                self.append_attr(elem_id, name, None, value)
        if not self.text_compare(xml_was.text, xml_then.text):
            self.append_text(elem_id, xml_was.text, xml_then.text)
        if not self.text_compare(xml_was.tail, xml_then.tail):
            self.append_text(elem_id, xml_was.tail, xml_then.tail)
        if elem_id in self:
            self[elem_id].elem = xml_then
            self[elem_id].prev_elem = xml_was

        # Record the lookup dictionary as this is safer than getElementById
        # for possible duplicate ids in the documents.
        lookup_a = OrderedDict([(elem.get_id(), elem) for elem in xml_was])
        lookup_b = OrderedDict([(elem.get_id(), elem) for elem in xml_then])
        children_a = list(lookup_a)
        children_b = list(lookup_b)

        matcher = SequenceMatcher(None, children_a, children_b)
        for tag, start_a, end_a, start_b, end_b in reversed(matcher.get_opcodes()):
            items_a = [lookup_a[eid] for eid in children_a[start_a:end_a]]
            items_b = [lookup_b[eid] for eid in children_b[start_b:end_b]]
            if tag == 'equal':
                for item_a, item_b in zip(items_a, items_b):
                    self._xmldiff(item_a, item_b)

            # We check for elements which have been ungrouped, moved around etc.
            if tag in ['delete', 'replace']:
                for item in items_a:
                    self._cmp_move(item, xml_then.root.getElementById(item.get_id()), False)
            if tag in ['insert', 'replace']:
                for item in items_b:
                    self._cmp_move(xml_was.root.getElementById(item.get_id()), item, True)

    def _cmp_move(self, child_a, child_b, is_create):
        """Id based compare once order difference is done"""
        if child_a is None:
            # Element B has been created
            self.create_tag(child_b)
        elif child_b is None:
            # Element A has been deleted
            self.delete_tag(child_a)
        else:
            # Moved, for example grouped, ungrouped etc
            self.move_tag(child_a, child_b, is_create)
            self._xmldiff(child_a, child_b)

    @staticmethod
    def text_compare(test1, test2):
        if not test1 and not test2:
            return True
        if test1 == '*' or test2 == '*':
            return True
        return (test1 or '').strip() == (test2 or '').strip()

    def create_tag(self, tag):
        self[tag.get_id()].append("CREATED")
        self[tag.get_id()].elem = tag

    def delete_tag(self, tag):
        self[tag.get_id()].append("DELETED")
        self[tag.get_id()].prev_elem = tag

    def move_tag(self, tag_was, tag_then, is_create):
        elem_id = tag_was.get_id()
        parent_was = tag_was.getparent()
        parent_then = tag_then.getparent()
        if parent_was.getparent().get_id() == parent_then.get_id():
            self[elem_id].append("UNGROUP")
        elif isinstance(parent_then, inkex.Group) \
                and tag_was.root.getElementById(parent_then.get_id()) is None:
            self[elem_id].append("GROUP")
        elif is_create:
            self[elem_id].append("MOVE")
        self[elem_id].elem = tag_then
        self[elem_id].prev_elem = tag_was

    def reorder_tag(self, tag_was, tag_then):
        elem_id = tag_was.get_id()
        self[elem_id].append("REORDERED")
        self[elem_id].elem = tag_then
        self[elem_id].prev_elem = tag_was

    def append_attr(self, elem_id, attr, value_a, value_b):
        """Record an attribute difference"""
        if attr == "style":
            self.append_style(elem_id, inkex.Style(value_a), inkex.Style(value_b))
        else:
            self[elem_id].append(DiffNode(attr, value_a, value_b))

    def append_style(self, elem_id, style_a, style_b):
        for key in set(style_a) | set(style_b):
            val_a = style_a.get(key, "")
            val_b = style_b.get(key, "")
            if val_a != val_b:
                self[elem_id].append(DiffNode(key, val_a, val_b));

    def append_text(self, elem_id, text_a, text_b):
        """Record a text difference"""
        self[elem_id].append(DiffNode("_text", text_a, text_b))

    def __bool__(self):
        """Returns True if there's no log, i.e. the delta is clean"""
        return not self.__len__()
    __nonzero__ = __bool__

    def __repr__(self):
        if self:
            return "No differences detected"
        return f"{len(self)} differences"

    def __str__(self):
        return "\n".join([_id + ":" + str(self[_id]) for _id in self])

    def get_all(self, tag):
        for d_el in self.values():
            if tag in d_el:
                yield d_el

class DiffElement(list):
    """A different in an element"""
    def __init__(self):
        self.elem_id = None
        self.elem = None
        self.prev_elem = None

    def __str__(self):
        return ";".join([str(attr) for attr in self])


class DiffNode:
    """A single different node"""
    def __init__(self, key, value_a, value_b):
        self.key = key
        self.value_a = value_a
        self.value_b = value_b

    def __str__(self):
        return f"{self.key}={self.value_a}>{self.value_b}"
