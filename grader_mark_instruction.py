#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 IMT (France)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# Author: Martin Owens <doctormo@geek-2.com>
#
"""
Mark the next available instruction (if any) and either:

    1. Check passed. Mark and advance to the next instruction
    2. Check failed. Mark as failed and provide help on canvas
"""

import inkex
from inkex.elements import load_svg
from inkex.units import parse_unit

from grader import __version__
from grader.core import GraderMixin
from grader.rules import Exam, BadExamRule

class MarkMoocInstruction(GraderMixin, inkex.EffectExtension):
    """Mark the next instruction"""

    def effect(self):
        try:
            self._effect()
        except BadExamRule as err:
            self.msg(f"This exam has a broken definition and can not be marked:\n\n{err}")

    def _effect(self):
        self.clear_errors()

        # 1. Find the latest instruction not-marked
        instruction = self.get_next_instruction()
        if instruction is None or not instruction.desc:
            return

        # 2. Make an exam from the instructions text
        self.exam = Exam(self.load_state(), self.svg)
        self.exam.append(instruction.desc)

        # 3. Check the rules
        if self.exam.has_passed():
            self.set_marked(instruction, True)
            _next = self.get_next_instruction(marked=False, visible=False)
            if _next is not None:
                self._show(_next)
            return

        # 4. Create errors on canvas to explain what went wrong.
        for rule, elem, crit in self.exam.failures:
            self.show_failure(rule, elem, crit)

    def show_failure(self, rule, elem, crit):
        """Show the failure on the canvas with a description"""
        if elem is None:
            # XXX Ask rule to auto_error_text here.
            error_text = "Could not find element!"
        else:
            error_text = crit.error_text or rule.error_text or crit.auto_error_text(elem)
        self.add_error(error_text, point=elem)

if __name__ == '__main__':
    MarkMoocInstruction().run()
