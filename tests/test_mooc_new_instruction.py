# coding=utf-8
#
# Copyright (C) 2021 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA.
#
"""Test creating a new instruction"""

from inkex.tester import ComparisonMixin, TestCase

from grader_new_instruction import NewMoocInstruction

class NewInstructionTest(ComparisonMixin, TestCase):
    effect_class = NewMoocInstruction
    compare_file = 'test_maker.svg'
    text = [
        'A simple text instruction which should be encoded, we know. But for which we need it to be long enough.',
        'The next instruction doesn\'t have to be big',
        'But the one after that is quite a bit bigger, as it does ask the user to do some amazing things.'
        'Finally we end with the instruction to mark their work.'
    ]

    def test_all_comparisons(self):
        """
        Chain together a bunch of instruction requests to emulate the creation of
        each instruction.
        """
        filename = self.compare_file
        for text in self.text:
            effect = self.assertEffect(filename, args=('--text', text))
            filename = self.temp_file()
            with open(filename, 'wb') as fhl:
                fhl.write(effect.test_output.getvalue())

        args = ('--text', 'EOF!')
        self.assertCompare(filename, self.get_compare_cmpfile(args, None), args)

