#
# Copyright 2021 - IMT Atlantique Bretagne
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Read in an svg template and provide a set of rendering features.
"""

import inkex
from inkex import load_svg
from inkex.styles import ConditionalRule
from inkex.elements._selected import ElementList

from .instructions import InstructionMixin
from .errors import ErrorsMixin

DOCUMENT_NODE = 'mooc:document'

class GraderMixin(InstructionMixin, ErrorsMixin):
    """
    Add this mixin to an extension, it must provide an svg object.
    """
    extra_nss = {
        'mooc': 'https://inkscape.org/namespaces/mooc',
    }

    def load_state(self):
        """
        Returns the previous version of the svg document, is available. Or returns None.
        """
        if self.svg.desc:
            return load_svg(self.svg.desc)

    def save_state(self):
        """
        Saves the document into itself.
        """
        # To prevent telescoping, we remove any previous state.
        self.delete_state()
        self.svg.desc = self.svg.tostring()

    def delete_state(self):
        """
        Delete any document state
        """
        del self.svg.desc

    def make_mooc(self, version=None):
        """Turn an empty document into a mooc document"""
        self.svg.set(DOCUMENT_NODE, version or 'yes')

    def is_mooc_document(self, version=None):
        """Returns true if this document is a mooc document"""
        if version:
            return self.svg.get(DOCUMENT_NODE) == version
        return bool(self.svg.get(DOCUMENT_NODE))

    def get_mooc_layer(self):
        elem = self.get_instruction('instruction_template')
        for anc in elem.ancestors():
            if isinstance(anc, inkex.Layer):
                return anc
        return None

    def is_mooc_element(self, elem):
        if elem is None:
            return False
        layer = self.get_mooc_layer()
        for anc in elem.ancestors():
            if anc == layer:
                return True
        return False

    def get_children(self, elem, cls=None):
        """Basic child filtering"""
        ret = elem
        if cls is not None:
            ret = elem.xpath(ConditionalRule(f".{cls}").to_xpath())
        lst = ElementList(self.svg, list(ret))
        return lst

    def get_child(self, elem, **kwargs):
        return self.get_children(elem, **kwargs).first()

    def set_title(self, title):
        """Sets the title for the mooc"""
        self._set_text('title_text', title)

    def _set_text(self, elem, text):
        """This will cope with both tspan and text flow bases text setting"""
        orig_elem = elem
        if isinstance(elem, str):
            elem = self.svg.getElementById(elem)

        if not isinstance(elem, (inkex.Tspan, inkex.TextElement)):
            elem = ElementList(self.svg, [elem]).get(inkex.TextElement).first()

        if isinstance(elem, inkex.TextElement):
            if elem.tspans():
                elem = elem.tspans()[0]

        if isinstance(elem, (inkex.Tspan, inkex.TextElement)):
            elem.text = text
        else:
            self.msg(f"Unknown element text {orig_elem}")

    def _show(self, elem, display=True):
        """Show or hide the given element"""
        if not display:
            elem.style['display'] = 'none'
        elif 'display' in elem.style:
            del elem.style['display']

