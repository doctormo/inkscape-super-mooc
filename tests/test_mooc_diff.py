# coding=utf-8
#
# Copyright (C) 2021 IMT (France)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# Author: Martin Owens <doctormo@geek-2.com>
#
"""Test diff module"""

import os
import sys

from grader.diff import SvgChanges

from inkex.tester import TestCase
from inkex import load_svg

class DiffCoreTest(TestCase):
    """Testing core functions"""
    maxDiff = 600002

    def assertDifference(self, svg_file, diff, layer=None):
        """Test the differences between two files are explained correctly"""
        changed = load_svg(self.data_file(os.path.join('diff', svg_file)))
        this_diff = SvgChanges(
            self.reference.getroot(),
            changed.getroot(),
            layer=layer
        )
        self.assertEqual(diff.strip(), str(this_diff).strip())

    def setUp(self):
        self.reference = load_svg(self.data_file('diff/reference.svg'))
        super().setUp()

    def test_add(self):
        self.assertDifference('a_add_rect.svg', """rect904:CREATED""", layer="layer_a")

    def test_remove(self):
        self.assertDifference('a_remove_circle.svg', """c1:DELETED""", layer="layer_a")

    def test_group(self):
        self.assertDifference('a_group_objs.svg', """
c1:GROUP
c2:GROUP
c3:GROUP
g40:CREATED
""", layer="layer_a")

    def test_ungroup(self):
        # Ungrouping text in inkscape involves moving text objects around too.
        self.assertDifference('b_ungroup_text.svg', """
t5:DELETED
text3762:UNGROUP;y=229.64807>100;x=54.289616>500
tspan3760:y=229.64807>100;x=54.289616>500
text3766:UNGROUP;y=259.64807>130;x=54.289619>500
tspan3764:y=259.64807>130;x=54.289619>500
""", layer="layer_b")

    def test_move(self):
        self.assertDifference('b_move_rect.svg',\
"""r1:x=100>725.90204;y=200>741.66479""", layer="layer_b")

    def test_reorder(self):
        self.assertDifference('b_reorder_rect.svg', """r2:MOVE""", layer="layer_b")

    def test_resize(self):
        self.assertDifference('b_resize_rect.svg',\
"""
r2:stroke-width=16>20.761;width=200>259.5127;height=100>129.75635;x=300>270.24365
""", layer="layer_b")


    def test_text_change(self):
        self.assertDifference('b_text_changed.svg',\
"""
flowPara2564:CREATED
flowPara20:_text=flow text which wraps>text changed
""", layer="layer_b")

