#
# Copyright 2021 - IMT Atlantique Bretagne
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""Functions related to managing instructions"""

import sys

class InstructionMixin(object):
    """The instructions focus of modifiying the document"""
    def get_instruction(self, by_id):
        """Returns the instruction template element"""
        return self.svg.getElementById(by_id)

    def get_instructions(self, visible=None):
        """Yields valid and available instructions"""
        for item in self.svg.getElementsByClass('instruction'):
            if item.eid != 'instruction_template' and item.eid != 'instruction_gap':
                if visible is None or visible == item.is_visible():
                    yield item
    
    def get_next_instruction(self, marked=False, visible=True):
        """Returns the next instruction.

         - Marked, Visible = Previous instruction successfully done.
         - Not-Marked, Visible = The current instruction the user is doing.
         - Not-Marked, Not-Visible = The next instruction the user will do.
         - Marked, Not-Visible = Shouldn't happen
        """
        #sys.stderr.write(f"get_next_instruction: marked:{marked}, visible:{visible}\n")
        ret = None
        for instruction in reversed(list(self.get_instructions(visible=visible))):
            oid = instruction.get_id()
            #sys.stderr.write(f" * Looking at instruction {oid}\n")
            is_marked = self.get_marked(instruction)
            #sys.stderr.write(f"     if is_marked:{is_marked} == marked:{marked}\n")
            if marked == is_marked:
                ret = instruction
                #sys.stderr.write(f"         if not (marked{marked} and visible{visible})\n")
                if not (marked and visible):
                    #sys.stderr.write("            break!\n")
                    break
        return ret

    def add_instruction(self, identifier, text, mr_instructions):
        """Add an instruction to the mooc document"""
        items = list(self.get_instructions())

        tmp = self.get_instruction('instruction_template')
        gap = self.get_instruction('instruction_gap')

        # Duplicate instruction group
        ins = tmp.duplicate()

        # Generate an instruction
        num = len(items)
        while '%d' in identifier:
            new_id = identifier % num
            if not self.svg.getElementById(new_id):
                identifier = new_id
                break
            else:
                num += 1

        # Set identifier of the new instruction
        ins.set_id(identifier)

        # Set text to the input text
        self._set_text(ins, text)

        # Set the description field to the new machine readable instructions
        ins.desc = mr_instructions

        # Checkmark this instruction
        self.set_marked(ins, False)

        # Set the position of the instruction has the number times gap
        gap_trans = gap.transform * -tmp.transform

        for i in range(num):
            ins.transform *= gap_trans

        self._show(ins, True)

    def set_marked(self, ins, marked):
        """Set the checkbox for this instruction"""
        if isinstance(ins, str):
            ins = self.get_instruction(ins)
        # make it visible or not depending on marked variable
        self._show(self.get_child(ins, cls='checkmark'), marked)

    def get_marked(self, ins):
        """Get if this instruction has been marked (sucessful)"""
        if isinstance(ins, str):
            ins = self.get_instruction(ins)
        return self.get_child(ins, cls='checkmark').is_visible()
