#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 IMT (France)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# Author: Martin Owens <doctormo@geek-2.com>
#
"""
Finish a class.
"""

import hashlib
from datetime import datetime

import inkex
from inkex.elements import load_svg
from inkex.units import parse_unit

from grader import __version__
from grader.core import GraderMixin

def now():
    return datetime.now()

class FinaliseMoocClass(GraderMixin, inkex.TemplateExtension):
    """Finish making a mooc class"""
    def effect(self):
        try:
            version = int(self.svg.get('inkscape:mooc_version', "1"))
        except ValueError:
            version = 1

        full_desc = ""
        ins = None
        # Instructions are in reverse order, so last one is first.
        for ins in self.get_instructions():
            self.set_marked(ins, False)
            self._show(ins, False)
            if ins.desc is not None:
                full_desc += ins.desc
        if ins is not None:
            self._show(ins, True)

        # Lock the information layer so students can't edit it.
        for elem in ins.ancestors().filter(inkex.Layer):
            elem.set('sodipodi:insensitive', 'true')

        # Set the selected layer to the workspace
        self.svg.namedview.set('inkscape:current-layer', "layer1")
        self.svg.getElementById("layer1").set('sodipodi:insensitive', None)

        # Calculate hash of all instructions
        self.svg.set('inkscape:mooc_version', version + 1)
        self.svg.set('inkscape:mooc_created', now().isoformat())
        self.svg.set('inkscape:mooc_hash', hashlib.md5(full_desc.encode("utf-8")).hexdigest())


if __name__ == '__main__':
    FinaliseMoocClass().run()
