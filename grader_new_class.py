#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 IMT (France)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# Author: Martin Owens <doctormo@geek-2.com>
#
"""
Create a new class based on the template file.
"""

import inkex
from inkex.elements import load_svg
from inkex.units import parse_unit

from grader import __version__
from grader.core import GraderMixin

class NewMoocClass(GraderMixin, inkex.TemplateExtension):
    """Create a new Mooc Class"""

    def add_arguments(self, pars):
        pars.add_argument("--title", help="Class title")
        pars.add_argument("--subtitle", help="Class subtitle")

    def effect(self):
        self.document = load_svg(self.get_resource('grader_new_class.svg'))
        self.svg = self.document.getroot()

        # Perform any initalisation tasks here
        self.make_mooc(version=__version__)
        self.set_title(self.options.title + "\n" + (self.options.subtitle or ''))
        self.clear_errors()

        # Set the first instruction text
        self._show(self.get_instruction('instruction_template'), False)
        self._show(self.get_instruction('instruction_gap'), False)
        self.add_instruction("meta-instruction",
            "Insert new instructions by modifying the document and going to 'Extensions > MOC > New Instruction'", "")

if __name__ == '__main__':
    NewMoocClass().run()
