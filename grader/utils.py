#
# Copyright 2021 - IMT Atlantique Bretagne
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Process for using rules to test changes in a document.
"""

import json
from collections import defaultdict

import inkex
from inkex.paths import Line
from inkex.units import convert_unit
from inkex.elements._base import NodeBasedLookup

class ElementTypes(dict):
    def build(self):
        for (ns, tag), classes in NodeBasedLookup.lookup_table.items():
            self[tag] = classes[0]
            # This captures things like 'layer' if needed
            for cls in classes[1:]:
                self[cls.__name__.lower()] = cls
        self['circle'] = (self['ellipse'], self['circle'])
        return self

    def __iter__(self):
        if not self:
            self.build()
        return super().__iter__()

    def __getitem__(self, name):
        if not self:
            self.build()
        return super().__getitem__(name)

    def __contains__(self, name):
        if not self:
            self.build()
        return super().__contains__(name)

element_types = ElementTypes()

def to_unit(arg=None, unit='px'):
    """Convert the numbers to the given unit, can be used as a decorator or directly"""
    # Decerator callers
    def _outer(fn):
        def _inner(*args, **kwargs):
            iunit = kwargs.pop('unit', unit)
            return fn(*to_unit(args, iunit), **to_unit(kwargs, iunit))
        return _inner

    # Use as decorator
    if callable(arg):
        return _outer(arg)
    if arg is None:
        return _outer

    # Use as regular caller
    if isinstance(arg, (str, int, float)):
        return convert_unit(arg, unit)
    if isinstance(arg, (list, tuple)):
        return [to_unit(item, unit) for item in arg]
    if isinstance(arg, dict):
        return dict([(name, to_unit(val, unit)) for name, val in arg.items()])
    raise ValueError("Not sure what this unit type is: {}".format(type(arg).__name__))

class Within:
    """Range tester"""
    def __init__(self, a, b):
        if type(a) != type(b):
            raise ValueError("Comparison types must be the same!")
        self.a, self.b = a, b

    def __contains__(self, other):
        return self.a <= other <= self.b

    def __str__(self):
        return f"{self.a} <= x <= {self.b}"

    def __repr__(self):
        return f"<Within a={self.a} b={self.b}>"

    @classmethod
    @to_unit
    def range(cls, x, delta=0.0):
        """Create a range from the given value and delta"""
        if isinstance(x, list) and len(x) == 2:
            return cls(*x)
        return cls(x - delta, x + delta)

    @classmethod
    def xy_range(cls, x, y, delta=None, y_max=None, unit=None, dd=0.0):
        """Return a range which can accept a list and return two ranges"""
        if y_max is None:
            if delta is None:
                delta = dd
            y_max = delta
        return cls.range(x, delta, unit=unit),\
               cls.range(y, y_max, unit=unit)


class Exam(list):
    """A list of rules loaded from a document"""
    def __init__(self, doc_was, doc_then):
        self.doc_was = doc_was
        self.doc_then = doc_then
        self.failures = []

    def append(self, rule):
        if isinstance(rule, ExamRubric):
            super().append(rule)
        elif isinstance(rule, list):
            for item in rule:
                self.append(rule)
        elif isinstance(rule, dict):
            try:
                self.append(ExamRubric(**rule))
            except TypeError as err:
                raise BadExamRule(f"Bad exam rubric inputs: {err}")
        elif isinstance(rule, str):
            rule = rule.strip()
            if not rule.startswith('['):
                rule = '[' + rule.strip(',') + ']'
            try:
                for item in json.loads(rule):
                    self.append(item)
            except json.decoder.JSONDecodeError as err:
                raise BadExamRule(f"Could not parse exam rule: {err}\n```\n{rule}\n```\n")
        else:
            kind = type(rule).__name__
            raise BadExamRule(f"Can't add {kind} of rule, unknown type.")

    def run_test(self):
        """Run each test in the exam and return the total score"""
        self.failures = []
        self.success = []
        for rule in self:
            result, elem, crit = rule.run_test(self.doc_was, self.doc_then)
            if not result:
                self.failures.append((rule, elem, crit))
            else:
                self.success.append((rule, elem))

    def has_passed(self):
        """Return true if this exam passes the rules"""
        if not self.failures:
            self.run_test()
        return not self.failures

class ExamRubric:
    def __init__(self, relabel=None, match=(), criteria=(), error_text=None):
        self.matcher = None
        self.label = relabel
        self.rules = []
        self.matcher = ObjectMatcher(match)
        self.criteria = [ExamCriteria(*r) for r in criteria if isinstance(r, (list, tuple))]
        self.error_text = error_text

    def run_test(self, doc_was, doc_then):
        """Runs this test on the current ontext in the exam"""
        elem = self.matcher.get_first(doc_then)
        if elem is None:
            return (False, None, None)
        # XXX It might be possible to add in here a way of getting the
        # element by id in doc_was and therefore allow delta tests too
        for crit in self.criteria:
            if not crit.test(elem):
                return (False, elem, crit)
        return (True, elem, None)

class ExamCriteria:
    def __init__(self, kind, *bits):
        self.kind = kind
        self.args = []
        self.kwargs = {}
        self.error_text = None
        for bit in bits:
            if isinstance(bit, list):
                self.args = bit
            elif isinstance(bit, dict):
                self.kwargs = bit
            elif isinstance(bit, str):
                self.error_text = bit
        # Set the testing function and be flexible
        self.func = getattr(self, f'test_{kind}')
        if self.kwargs and not self.args:
            self.func = getattr(self, f'test_{kind}_kw', self.func)

    def test(self, elem):
        """Test this element against the critiria"""
        return self.func(elem, *self.args, **self.kwargs)

    @staticmethod
    def test_size(elem, width, height, delta=1.0):
        """Size is wrong"""
        return ExamCriteria.test_size_kw(elem, width=width, height=height, delta=delta)

    @staticmethod
    def test_pos(elem, left, top, delta=1.0):
        """Position is wrong"""
        return ExamCriteria.test_size_kw(elem, left=left, top=top, delta=delta)

    @staticmethod
    def test_size_kw(elem, delta=1.0, **kwargs):
        """Size is wrong"""
        bbox = elem.shape_box(transform=elem.composed_transform())
        for label in kwargs:
            if getattr(bbox, label) not in Within.range(kwargs[label], delta):
                return False
        return True

    @staticmethod
    def test_pos_kw(elem, **kwargs):
        """Position is wrong"""
        return ExamCriteria.test_size_kw(elem, **kwargs)

    @staticmethod
    def test_center(elem, cx, cy, delta=1.0):
        """Center in wrong position"""
        return ExamCriteria.test_size_kw(elem, center_x=cx, center_y=cy, delta=delta)

    @staticmethod
    def test_style(elem, **kwargs):
        """Style is wrong"""
        for label in kwargs:
            pass

    @staticmethod
    def test_path(elem, subpaths=None, nodes=None):
        """Shape is wrong"""
        path = elem.path
        if subpaths is not None:
            subpaths = int(Within.range(subpaths, delta=0))
            #if subpaths

    def auto_error_text(self, elem):
        """Generate an error text from the element"""
        return self.func.__doc__ or "Unknown error!"

class ObjectMatcher:
    def __init__(self, matchings):
        self.mat = matchings

    @staticmethod
    def _get_descendants(svg):
        for item in svg.descendants():
            if any([isinstance(anc, inkex.Defs) for anc in item.ancestors()]):
                continue
            yield item

    def get_objects(self, svg):
        """Return the matched objects based on the matchings, returns a generator of score, element pairs"""
        total = defaultdict(int)
        for key, val in self.mat.items():
            for elem, score in getattr(self, f'get_by_{key}')(svg, val):
                total[elem] += score

        for elem in sorted(total, key=lambda k: total[k], reverse=True):
            yield (elem, total[elem])

    def get_first(self, svg):
        """Return the highest matching object"""
        for elem, score in self.get_objects(svg):
            return elem

    def get_by_type(self, svg, val):
        """Find all elements of a specific type"""
        if val not in element_types:
            raise KeyError(f"Unknown element type: '{val}'")
        for elem in self._get_descendants(svg):
            if isinstance(elem, element_types[val]):
                yield (elem, 1.0)

    @staticmethod
    def get_by_size(svg, size, x='width', y='height'):
        """Find all elements that are aprox the given size"""
        x_range, y_range = Within.xy_range(*size, unit=svg.unit)
        for elem in ObjectMatcher._get_descendants(svg):
            try:
                bbox = elem.shape_box(transform=elem.composed_transform())
            except AttributeError:
                continue
            if getattr(bbox, x) in x_range and getattr(bbox, y) in y_range:
                # Could adjust score based on proximity to test.
                yield (elem, 1.0)

    def get_by_pos(self, svg, pos):
        return self.get_by_size(svg, pos, x='left', y='top')

    def get_by_center(self, svg, pos):
        return self.get_by_size(svg, pos, x='center_x', y='center_y')

    def get_by_label(self, svg, label):
        for elem in svg.descendants():
            if elem.label == label or elem.get_id() == label:
                yield (elem, 10.0) # High score, because it's certain

from inkex import BoundingBox

def mult(box1, box2):
    """we have to do our own intersection because BoundBox doesn't do it yet."""
    x_left = max(box1.left, box2.left)
    y_top = max(box1.top, box2.top)
    x_right = min(box1.right, box2.right)
    y_bottom = min(box1.bottom, box2.bottom)
    if x_right < x_left or y_bottom < y_top:
        return BoundingBox()
    return BoundingBox((x_left, x_right), (y_top, y_bottom))

def intersection(box1, box2):
    ins = mult(box1, box2)
    box1_area = box1.width * box1.height
    ins_area = ins.width * ins.height
    print("{0} / ({1} - {0}) = {2}".format(int(ins_area), int(box1_area), ((box1_area - ins_area) / ins_area)))
    return 1.0 - ((box1_area - ins_area) / ins_area)

