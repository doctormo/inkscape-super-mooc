# coding=utf-8
#
# Copyright (C) 2021 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA.
#
"""Test marking instructions"""

from inkex.tester import ComparisonMixin, TestCase

from grader_mark_instruction import MarkMoocInstruction

class MarkInstructionTest(ComparisonMixin, TestCase):
    effect_class = MarkMoocInstruction
    comparisons = []
    stdout_protect = False
    stderr_protect = False

    def test_next_instruction(self):
        """Test the get next instruction function"""
        effect = self.assertEffect(self.data_file('marks/00-listings.svg'))
        self.assertEqual(effect.get_next_instruction(marked=True, visible=True).get_id(), 'AlreadyDone')
        self.assertEqual(effect.get_next_instruction(marked=False, visible=True).get_id(), 'NextToDo')
        self.assertEqual(effect.get_next_instruction(marked=False, visible=False).get_id(), 'FutureTask')
        self.assertEqual(effect.get_next_instruction(marked=True, visible=False), None)

    def test_basic_advancement(self):
        """Test a file that always passes, advances to the next"""
        effect = self.assertEffect(self.data_file('marks/01-basic.svg'))
        self.assertTrue(hasattr(effect, 'exam'))
        self.assertTrue(effect.exam.has_passed())
        self.assertEqual(effect.exam.failures, [])
        ins1 = effect.svg.getElementById('ins-1')
        self.assertNotIn('display', ins1.style)
        ins0 = effect.svg.getElementById('ins-0-checkmark')
        self.assertNotIn('display', ins0.style)
        ins1 = effect.svg.getElementById('ins-1-checkmark')
        self.assertEqual(ins1.style['display'], 'none')

    def test_pointing_errors(self):
        """Testing pointing out errors"""
        effect = self.assertEffect(self.data_file('marks/02-known-failure.svg'))
        self.assertTrue(hasattr(effect, 'exam'))
        self.assertFalse(effect.exam.has_passed())
        self.assertEqual(len(effect.exam.failures), 4)

        data_a = effect.test_output.getvalue()
        filename = self.get_compare_cmpfile(('--failures',), None)
        with open(filename, 'rb') as fhl:
            data_b = fhl.read()
        with open(filename + '.svg', 'wb') as fhl:
            fhl.write(data_a)
        self._base_compare(data_a, data_b, None)

