# coding=utf-8
#
# Copyright (C) 2021 IMT (France)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# Author: Martin Owens <doctormo@geek-2.com>
#
"""Test Rules Module"""

from grader.rules import Exam, ExamRubric, ExamCriteria, ObjectMatcher, BadExamRule

from inkex.tester import TestCase
from inkex import load_svg

class ExamTest(TestCase):
    """Test an Exam object"""
    def setUp(self):
        self.svg = load_svg(self.data_file('test_rules.svg')).getroot()
        self.exam = Exam(None, self.svg)
        super().setUp()

    def test_exam(self):
        self.exam.append("""
{
    "match": {"label": "BigSquare"},
    "criteria": [
        ["size", {"width": "100mm", "height": "100mm"}]
    ]
}""")
        self.assertTrue(self.exam.has_passed())
        self.assertEqual(len(self.exam.success), 1)

    def test_exam_errors(self):
        self.assertRaises(BadExamRule, self.exam.append, "&")
        self.assertRaises(BadExamRule, self.exam.append, 12)
        self.assertRaises(BadExamRule, self.exam.append, '{"nope": "A"}')


class ExamRubricTest(TestCase):
    """Test Rubic"""
    def setUp(self):
        self.svg = load_svg(self.data_file('test_rules.svg')).getroot()
        self.svg.selection.set('tiny_square')
        super().setUp()

    def test_rubric_success(self):
        rubric = ExamRubric(match={'label': 'BigSquare'}, criteria=(
            ('size', {'width': '100mm', 'height': '100mm'}),
        ))
        (success, elem, crit) = rubric.run_test(None, self.svg)
        self.assertTrue(success)
        self.assertEqual(elem.get_id(), 'big_square')
        self.assertEqual(elem.label, 'BigSquare')

    def test_rubric_failure(self):
        rubric = ExamRubric(match={'label': 'BigSquare'}, criteria=(
            ('size', {'width': '10mm', 'height': '10mm'}, "err"),
        ))
        (success, elem, crit) = rubric.run_test(None, self.svg)
        self.assertFalse(success)
        self.assertEqual(elem.get_id(), 'big_square')
        self.assertEqual(crit.error_text, "err")

    def test_rubric_relabel(self):
        """Relabeling the found element"""
        rubric = ExamRubric(match={'type': 'rect', 'size': ['10mm', '10mm', '1mm']}, criteria=(
            ('size', {'height': '10mm'}),
        ), relabel="OneHundred")
        (success, elem, crit) = rubric.run_test(None, self.svg)
        self.assertEqual(elem.get_id(), 'tiny_square')
        self.assertEqual(elem.label, 'OneHundred')
        self.assertTrue(success)

    def test_rubric_no_match(self):
        rubric = ExamRubric(match={'label': 'NotASquare'}, criteria=())
        (success, elem, crit) = rubric.run_test(None, self.svg)
        self.assertFalse(success)
        self.assertEqual(elem, None)

    def test_rubric_selection(self):
        rubric = ExamRubric(match={'label': 'tiny_square'}, criteria=(
            ('selected', True),
        ))
        (success, elem, crit) = rubric.run_test(None, self.svg)
        self.assertTrue(success)
        rubric = ExamRubric(match={'label': 'tiny_square'}, criteria=(
            ('selected', False),
        ))
        (success, elem, crit) = rubric.run_test(None, self.svg)
        self.assertFalse(success)
        rubric = ExamRubric(match={'label': 'big_square'}, criteria=(
            ('selected', True),
        ))
        (success, elem, crit) = rubric.run_test(None, self.svg)
        self.assertFalse(success)


class ExamCriteriaTest(TestCase):
    """Test the exam criteria"""
    def setUp(self):
        self.svg = load_svg(self.data_file('test_rules.svg')).getroot()
        super().setUp()

    def test_size(self):
        crit = ExamCriteria('size', {'width': '100mm', 'height': '100mm'})
        self.assertTrue(crit.test(self.svg.getElementById('big_square')))
        self.assertFalse(crit.test(self.svg.getElementById('tiny_square')))

    def test_pos(self):
        crit = ExamCriteria('pos', {'left': 75, 'top': 75, 'delta': 2})
        self.assertTrue(crit.test(self.svg.getElementById('tiny_square')))
        self.assertFalse(crit.test(self.svg.getElementById('big_square')))
        crit = ExamCriteria('pos', [75, 75, 2])
        self.assertTrue(crit.test(self.svg.getElementById('tiny_square')))
        self.assertFalse(crit.test(self.svg.getElementById('big_square')))

    def test_center(self):
        crit = ExamCriteria('center', {'cx': '60mm', 'cy': '60mm', 'delta': '10mm'})
        self.assertTrue(crit.test(self.svg.getElementById('big_square')))
        self.assertFalse(crit.test(self.svg.getElementById('tiny_square')))

    def test_alignment_topleft(self):
        crit = ExamCriteria('pos', {'top': '#align_rect', 'left': '#align_rect', 'delta': '0.2mm'})
        self.assertTrue(crit.test(self.svg.getElementById('align_rect')))
        self.assertFalse(crit.test(self.svg.getElementById('align_circle')))

    def test_alignment_center(self):
        crit = ExamCriteria('center', {'cx': '#align_cross', 'cy': '#align_cross', 'delta': '0.2mm'})
        self.assertTrue(crit.test(self.svg.getElementById('align_circle')))
        self.assertFalse(crit.test(self.svg.getElementById('align_rect')))

    def test_alignment_size(self):
        # Test that the width is the same as vertical height (crossing params is ok!)
        crit = ExamCriteria('size', {'width': '#align_vert:height', 'delta': '0.1mm'})
        self.assertTrue(crit.test(self.svg.getElementById('align_horz')))
        self.assertFalse(crit.test(self.svg.getElementById('align_rect')))

    def test_overlap(self):
        crit = ExamCriteria('overlap', {'#align_circle': 1.0})
        self.assertTrue(crit.test(self.svg.getElementById('align_cross')))
        #self.assertTrue(crit.test(self.svg.getElementById('align_rect')))
        #self.assertFalse(crit.test(self.svg.getElementById('big_square')))

    def test_node_count(self):
        crit = ExamCriteria('path', {'count': [5, 10]})
        self.assertTrue(crit.test(self.svg.getElementById('nine_point')))
        self.assertFalse(crit.test(self.svg.getElementById('four_point')))
        crit = ExamCriteria('path', {'nodes': [10, 4, [3, 5]]})
        self.assertFalse(crit.test(self.svg.getElementById('nine_point')))
        self.assertTrue(crit.test(self.svg.getElementById('three_paths')))

    def test_node_subpaths(self):
        crit = ExamCriteria('path', {'subpaths': 1})
        self.assertTrue(crit.test(self.svg.getElementById('nine_point')))
        crit = ExamCriteria('path', {'subpaths': 3})
        self.assertTrue(crit.test(self.svg.getElementById('three_paths')))
        self.assertFalse(crit.test(self.svg.getElementById('nine_point')))

    def test_lpe(self):
        crit = ExamCriteria('lpe', {'effect': 'fillet_chamfer'})
        self.assertTrue(crit.test(self.svg.getElementById('cham')))
        self.assertFalse(crit.test(self.svg.getElementById('nine_point')))

    def test_lpe_attrs(self):
        crit = ExamCriteria('lpe', {'effect': 'fillet_chamfer', 'flexible': 'false'})
        self.assertTrue(crit.test(self.svg.getElementById('cham')))
        crit = ExamCriteria('lpe', {'effect': 'fillet_chamfer', 'flexible': 'true'})
        self.assertFalse(crit.test(self.svg.getElementById('cham')))

class ObjectMatcherTest(TestCase):
    """Test Object Matcher"""
    maxDiff = 600002

    def setUp(self):
        self.svg = load_svg(self.data_file('test_rules.svg')).getroot()
        super().setUp()

    def assertObjectList(self, lst, *ids):
        """Assert an object list"""
        lst = [(elem.get_id(), score) for elem, score in lst]
        self.assertEqual(len(lst), len(ids), f"List {lst} doesn't match {ids}")
        for (a_id, b_id) in zip(lst, ids):
            # Ignore score if we're not testing the scores
            if not isinstance(b_id, tuple):
                a_id = a_id[0]
            self.assertEqual(a_id, b_id)

    def test_ignore_locked(self):
        self.assertObjectList(
            ObjectMatcher.get_by_label(self.svg, "never"),
        )

    def test_get_by_label(self):
        self.assertObjectList(
            ObjectMatcher.get_by_label(self.svg, "TinySquare"),
            'tiny_square',
        )
        self.assertObjectList(
            ObjectMatcher.get_by_label(self.svg, "text_line"),
            'text_line',
        )

    def test_get_by_size(self):
        self.assertObjectList(
            ObjectMatcher.get_by_size(self.svg, ('100mm', '100mm', 2)),
            'big_square',
        )
        self.assertObjectList(
            ObjectMatcher.get_by_size(self.svg, (377, 377, '1mm')),
            'big_square',
        )
        self.assertObjectList(
            ObjectMatcher.get_by_size(self.svg, ((375, 379), (375, '101mm'))),
            'big_square',
        )
        self.assertObjectList(
            ObjectMatcher.get_by_size(self.svg, (-1, -1, -1)),
        )

    def test_get_by_center(self):
        self.assertObjectList(
            ObjectMatcher.get_by_center(self.svg, ('60mm', '60mm', '10mm')),
            'big_square',
        )

    def test_get_by_pos(self):
        self.assertObjectList(
            ObjectMatcher.get_by_pos(self.svg, (75, 75, 2)),
            'tiny_square',
        )

    def test_get_by_type(self):
        self.assertObjectList(
            ObjectMatcher.get_by_type(self.svg, "rect"),
            'big_square', 'tiny_square', 'align_rect',
        )

    def test_get_first(self):
        obj = ObjectMatcher({'label': 'BigSquare'}).get_first(self.svg)
        self.assertEqual(obj.get_id(), 'big_square')
        obj = ObjectMatcher({'label': 'doesnt exist'}).get_first(self.svg)
        self.assertEqual(obj, None)
        obj = ObjectMatcher({'type': 'rect', 'size': ['10mm', '10mm', '0.2mm']})\
                .get_first(self.svg)
        self.assertEqual(obj.get_id(), 'tiny_square')

    def test_get_objects(self):
        self.assertObjectList(
            ObjectMatcher({'size': ('100mm', '100mm', '1mm'), 'pos': (4, 4), 'type': 'rect'}).get_objects(self.svg),
            ('big_square', 2.0), ('tiny_square', 1.0), ('align_rect', 1.0),
        )
        self.assertObjectList(
            ObjectMatcher({'size': ('10mm', '10mm', '0.2mm'), 'pos': (75, 75, 1), 'type': 'rect'}).get_objects(self.svg),
            ('tiny_square', 3.0), ('big_square', 1.0), ('align_rect', 1.0),
        )

